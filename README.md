# nanopro
solid-state nanopore data analysis tool for analysing DNA with proteins (dCas9, streptavidin) and DNA structures (dumbells, DNA flowers)

** installation time: ** 5-15 minutes for python and jupyter notebook
run time for code execution <1 minutes

** requirements: ** requirements can be found in requirements.txt
** demo: **
expected output will vary depending on the experiment and python code ran. Files which can be used for demo in relation to the associated project can be found in the repository described on the paper associated with the code. Feel free to contact the corresponding authors with questions in relation to running the code.*

The below instructions show an installation from scratch, including Python3:

Download the latest Python3 version for windows from https://www.python.org/downloads/windows/ or for macOs from https://www.python.org/downloads/macos/

Start the installation, choose "Customize installation"
In "Optional Features", tick "pip" and "py launcher", click "Next"
In "Advanced Options", tick "Associate files with Python" and "Precompile standard library", click "Install"

Install jupyter-notebook using the following link: https://jupyter.org/install

Once the Python installation has finished, open the command line by typing "cmd" in the start menu
Navigate to the nanopro base directory using 'cd' and open up jupyter-notebook
